import re

g_sign2value = { 'N' : 1 , 'S' : -1, 'E' : 1, 'W' : -1 }
g_value2sign_y = { 1: 'N', -1: 'S' }
g_value2sign_x = { 1: 'E', -1: 'W' }


def get_coordinates(filename):
    y_label, x_label = re.findall('[A-Z]+',filename)
    y,x = re.findall('\d+',filename)
    return (y_label, x_label), (int(y), int(x))

def filename_base(filename):
    return filename.split('/')[-1].split('.')[0]

def to_filename(labels, coordinates):
    y_label, x_label = labels
    y,x = coordinates
    return '{0}{1}{2}{3}'.format(y_label, str(y).zfill(2), x_label, str(x).zfill(3))

def get_abs_coordinates(filename):
    # get items
    labels, coordinates = get_coordinates(filename)
    # based on labels, modify coordinates
    abs_coordinates = (coordinates[0]*g_sign2value[labels[0]], coordinates[1]*g_sign2value[labels[1]])
    return abs_coordinates

def abs_to_filename(coordinates):
    # convert abolute coordinates to (labels, coordinates) combo
    #   call to_filename() with it
    y,x = coordinates
    labels = g_value2sign_y[y//abs(y)], g_value2sign_x[x//abs(x)]
    coordinates = (int(abs(y)),int(abs(x)))
    return to_filename(labels, coordinates)

def get_full_filename(filename_base, path):
    return path + filename_base + '.hgt'
