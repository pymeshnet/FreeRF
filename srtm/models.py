import math
import os

import numpy as np
from scipy.misc import imresize, imsave
from matplotlib import pyplot as plt

import utils 

plt.style.use('ggplot')

# global constants
TILE_SIZE = 1201

class Tile(object):
    '''
    what are the properties of a tile?

    1. .shape    : dimensions 
    2. .location : location as absolution coordinates->(x,y)
    3. .location_str : location as string

    operations on tile?

    1. save(ext='png') : save to png/tiff
    2. view(resol=25, cmap='gray')  : view in low res
    3. threshold()     : > 5
    4. __str__         : print tile with coordinates, %land and %sea
    5. statistics      : mean, mode, histogram

    '''
    def __init__(self,filename,name='unnamed'):
        # Note : filename should include file path
        self.labels, self.location = utils.get_coordinates(filename)
        self.location_str = utils.filename_base(filename)
        self.filename = filename
        # get data from file
        self.data = self.get_data()
        # get shape and location from filename
        self.shape = self.data.shape

    def get_data(self,source=None):
        ''' get data from (.hgt) file '''
        if not source:
            source = self.filename
        # if file doesn't exist, return zeros : sea
        if not os.path.isfile(source):
            return np.zeros([TILE_SIZE,TILE_SIZE])
        siz = os.path.getsize(source)
        dim = int(math.sqrt(siz/2))
        assert dim*dim*2 == siz, 'Invalid file size'
        return np.fromfile(source, np.dtype('>i2'), dim*dim).reshape((dim, dim))

    def save(self, ext='jpg', scale=25): # use scale
        #imsave(self.location_str + '.' + ext, imresize(self.data,scale))
        thresh_data = self.threshold(imresize(self.data,scale))
        imsave(self.location_str + '.' + ext, thresh_data)

    def view(self, scale=25, cmap=None):
        plt.imshow(self.threshold(imresize(self.data, scale)))
        #plt.imshow(self.threshold(self.data), cmap=cmap)
        plt.show()

    def threshold(self, thresh=0):
        thresh_data = np.copy(self.data)
        idx = thresh_data< 5
        thresh_data[idx] = 255
        return thresh_data

    def statistics(self, log=True):
        #mean, stddev, variance, mode = 
        #   TODO : complete this
        pass

    def __str__(self):
        count = self.data.shape[0] * self.data.shape[1]
        land  = np.count_nonzero(self.data)/count
        water = 1.0 - land
        return '{0} : {1}% land and {2}% water'.format(self.location_str, round(land,4), round(water,4))


class Grid(Tile):
    '''
    - Grid is grid of tiles
    -  inherits some properties of Tile object

    <Inherited>

    Properties:
    1. .shape
    2. .location : centroid (the tile at the centre)
    3. .location_str : centroid as string

    Operations
    1. save(ext='png') : save to png/tiff
    2. view(resol=25, cmap='gray')  : view in low res
    3. threshold()     : > 5
    4. __str__         : print centroid tile with coordinates, %land and %sea
    5. statistics      : mean, mode, histogram
    

    <Mutated>

    Properties:
    1. .zoom_level : 1,3,5,...
    2. .num_cells  : (zoom_level)^2
    3. .grid_map   : a small [ zoom_level x zoom_level] matrix containing tile coordinates
    4. .path       : /path/to/.hgt_files

    Operations
    1. ???

    '''

    def __init__(self, location_str, path, zoom_level=1):
        # check if zoom level is odd
        if not zoom_level%2:
            raise ValueError('ERROR: Zoom level cannot be even')
        # from NxxSyyy, get coordinates and labels
        self.labels, self.location = utils.get_coordinates(location_str)
        self.location_str = location_str
        # set zoom level
        self.zoom_level = zoom_level
        # get absolute coordinates
        self.abs_coordinates = utils.get_abs_coordinates(location_str)
        # generate a grid map based on centroid/location and zoom_level
        self.grid_map = self.gen_grid_map()
        # maintain the path; we need to fetch other tiles
        self.path = path
        # number of cells; just here for consistency
        self.num_cells = self.zoom_level**2
        # get grid data
        self.data = self.get_grid_data()

    def get_grid_data(self):
        # use get_data to get the tiles in grid_map
        #   utils.to_filename() 
        # set grid_data to zeros
        grid_data = np.zeros([self.zoom_level*TILE_SIZE, self.zoom_level*TILE_SIZE])
        for i in range(self.zoom_level):
            for j in range(self.zoom_level):
                # get the coordinates (absolute)
                y = self.grid_map[0,i,j]
                x = self.grid_map[1,i,j]
                # get the filename
                filename = utils.abs_to_filename((y,x))
                # get the data
                data = self.get_data(source=utils.get_full_filename(filename,
                    self.path))
                # assign data to a slice of grid_data
                grid_data[i*TILE_SIZE: (i+1)*TILE_SIZE, 
                        j*TILE_SIZE:(j+1)*TILE_SIZE] = data
        return grid_data
     

    def gen_grid_map(self):
        # based on zoom level and the location/centroid
        y,x = self.abs_coordinates
        # get distance to edge of grid
        edge_distance = (self.zoom_level)//2
        # calculate maximum and minimum coordinates
        ymin, xmin = y - edge_distance, x - edge_distance
        ymax, xmax = y + edge_distance, x + edge_distance
        # an empty grid map 
        grid_map = np.zeros([2,self.zoom_level,self.zoom_level])
        # iterate through rows, columns 
        for i in range(self.zoom_level):
            for j in range(self.zoom_level):
                grid_map[0,i,j] = ymax - i
                grid_map[1,i,j] = xmin + j
        return grid_map
